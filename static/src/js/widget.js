//declaramos nuestro widget
odoo.define('pos_ext_ext.widgets', function (require) {
    "use strict";
    //heredamos o llamamos basewidget y web.core 
    var PosBaseWidget = require('point_of_sale.BaseWidget');
    var core = require('web.core');
    //esta t es kuna variable para motrar texto 
    var _t = core._t;
    //declaramos el nombre de nuestro widget
    var DeliveryNameWidget = PosBaseWidget.extend({     
       template: 'DeliveryNameWidget',  //nombre del widget
       //funckion para renderizar el widget
       renderElement: function () {
            var self = this;
            this._super();
            this.$el.click(function () {
                self.click_delivery();
            });
        },
        //funcion para cuando seleccionas una opcion de la lista del widget
        click_delivery: function () {
            var self = this;//aqui llamos a los eventos de gui.js
            this.gui.select_delivery({}).then(function (delivery) {
                self.pos.set_delivery(delivery);//establecer valor
                self.renderElement();//renderizar elemento
            });
        },
        //funcion para obtener un texto
        get_name: function () {
            var delivery = this.pos.get_delivery();//obtener el valor actual de la fecha de envio
            if (delivery) {
                return delivery;
            }
            return _t("Fecha de Envio");
        },
    });
    return {
        DeliveryNameWidget: DeliveryNameWidget,
    };
});