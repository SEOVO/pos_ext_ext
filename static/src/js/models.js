//definir nuestromodel.js
odoo.define('pos_ext_ext.models', function (require) {
    "use strict";
    //heredar models
    var models = require('point_of_sale.models');
    //llamar a la variable que maneja los datos de impresion del tikcket
    var _super_order = models.Order.prototype;   
    // Hacer que la fecah de envio perdure en la sesion
    models.PosModel = models.PosModel.extend({
        //metodo para obtener la fecha de envio
        get_delivery: function () {
            return this.get('current_delivery') ||
                this.db.load('current_delivery');
        },
        //metodo para establecer la fecha de envio
        set_delivery: function (delivery) {
            //esta condicion hace que si el numero tiene un digito
            //coloque un cero adelante
            if(delivery){
              var dd = delivery.split("/");
              var dia = dd[0];
              var mes = dd[1];
              if(dia.length==1){
                dia = '0'+dia;
              }
              if(mes.length==1){
                mes = '0'+mes;
              }
              var dx = dia+'/'+mes+'/'+dd[2];
            }else{
                var dx = delivery;
            }
            //gurdar la fecha
            this.set('current_delivery', dx);
            this.db.save('current_delivery', dx || null);
        },
    });

    //añadir la fecha  para que se pueda kimprimier en el ticket
    models.Order = models.Order.extend({
        export_for_printing: function () {
            var res = _super_order.export_for_printing.apply(this, arguments);
            res.delivery = this.pos.get_delivery();
            return res;
        },

        export_as_JSON: function () {
            var json = _super_order.export_as_JSON.apply(this, arguments);
            var okk = this.pos.get_delivery();
	    if(okk){
            var ok = okk.split('/');
            var delivery = ok[2]+'/'+ok[1]+'/'+ok[0];
               }else{
	        var delivery = okk;
	       }
            json.delivery_date = delivery ? delivery : false;
            return json;
        }, 
    });

});
