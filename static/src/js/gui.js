odoo.define('pos_place.gui', function (require) {
    "use strict";
    //heredar gui
    var gui = require('point_of_sale.gui');
    var core = require('web.core');
    var _t = core._t;

    gui.Gui.include({
    	select_delivery: function () {
            //variable donde se guardan los cambios
    		var def = new $.Deferred();
            var current_delivery = this.pos.get_delivery();//obtener la fecha de envio actual

        //Inicio del codigo para obtener la data para el calendario
            var datei = new Date();
            var contador = 0;
            var rango = 5;

            //obtener desde el año actual hasta 5 años despues
            var list = [];
            //var listr ;
            var anoi= datei.getFullYear();
            var ac = 0;
            var mc = 0;
            if(current_delivery){

            var cc = current_delivery.split('/');
             ac = parseInt(cc[2]);
             mc = parseInt(cc[1])-1;
            }else{
                ac = anoi;
                mc = datei.getMonth();
            }
            for (var i = anoi ; i <= (anoi+rango); i++) {
                //listr[i]=[];
                //obtener los dias de cada mes
                for (var f = 0 ; f <= 11; f++) {
                    //listr[i][f]=[];
                    var date = new Date();
                    var primerDia = new Date(i, f, 1);
                    var ultimoDia = new Date(i, f+ 1, 0);
                    for (var g = primerDia.getDate() ; g <= ultimoDia.getDate(); g++) {
                     //var item = this.pos.places[i];
                        list.push({
                         'label': g,
                         'item':  g+'/'+(f+1)+'/'+i,
                        });
                        
                    }

                }       
               
            }

        // fin de la data del calendario

        //ejecutamos la ventana modal
    		this.show_popup('selection', {
                title: _t("Seleccione una Fecha"),
                list: list, //lista a mostrar 
                //funcion cuando estableces una opcion
                confirm: function (delivery) {
                    def.resolve(delivery);
                },
                //funcion cuando haces clcik en cancelar
                cancel: function () {
                    def.resolve(null);
                },
                //funcion para resaltar el vaklor actual
                is_selected: function (delivery) {
                    
                    if(delivery){
                        var dd = delivery.split("/");
                        var dia = dd[0];
                       var mes = dd[1];
                       if(dia.length==1){
                          dia = '0'+dia;
                        }
                      if(mes.length==1){
                          mes = '0'+mes;
                        }

                        var dx = dia+'/'+mes+'/'+dd[2];
                    }else{
                       var dx = delivery;
                    }

     
                    if (current_delivery) {
                        return dx === current_delivery;
                    }

                    return false;

                },
            });
            //rordenar la lista y aplicar css 

            var dias =  '<div  style=" '+
                        'width: calc(14.2857% - 32px);font-size: 16px;line-height: 50px;'+
                        'background-color: #5e5959;color: white;'+
                        'padding: 0px 16px;height: 2.5rem;"> D</div>'+

                        '<div  style=" '+
                        'width: calc(14.2857% - 32px);font-size: 16px;line-height: 50px;'+
                        'background-color: #5e5959;color: white;'+
                        'padding: 0px 16px;height: 2.5rem;"> L</div>'+

                        '<div  style=" '+
                        'width: calc(14.2857% - 32px);font-size: 16px;line-height: 50px;'+
                        'background-color: #5e5959;color: white;'+
                        'padding: 0px 16px;height: 2.5rem;"> M</div>'+

                        '<div  style=" '+
                        'width: calc(14.2857% - 32px);font-size: 16px;line-height: 50px;'+
                        'background-color: #5e5959;color: white;'+
                        'padding: 0px 16px;height: 2.5rem;">X</div>'+

                        '<div  style=" '+
                        'width: calc(14.2857% - 32px);font-size: 16px;line-height: 50px;'+
                        'background-color: #5e5959;color: white;'+
                        'padding: 0px 16px;height: 2.5rem;"> J</div>'+

                        '<div  style=" '+
                        'width: calc(14.2857% - 32px);font-size: 16px;line-height: 50px;'+
                        'background-color: #5e5959;color: white;'+
                        'padding: 0px 16px;height: 2.5rem;"> V</div>'+

                        '<div  style=" '+
                        'width: calc(14.2857% - 32px);font-size: 16px;line-height: 50px;'+
                        'background-color: #5e5959;color: white;'+
                        'padding: 0px 16px;height: 2.5rem;"> S</div>'

           
            $('.popup header').append('<select id="sano" class="aa" name="oki" qq="0"> </select>');
            
            for (var i = anoi ; i <= (anoi+rango); i++) {
                $('.popup .scrollable-y').append("<div class='"+i+"'> <div class='cuerpo'> "
                    +'<div class="mes mes0 enero"> <div> Enero - '+i+' </div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div></div>'
                    +'<div class="mes mes1 febrero"><div> Febrero - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'
                    +'<div class="mes mes2 marzo"><div> Marzo - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'
                    +'<div class="mes mes3 abril"><div> Abril - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'
                    +'<div class="mes mes4 mayo"><div> Mayo - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'
                    +'<div class="mes mes5 junio"><div> Junio - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'
                    +'<div class="mes mes6 julio"><div> Julio - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'
                    +'<div class="mes mes7 agosto"><div> Agosto - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'
                    +'<div class="mes mes8 setiembre"><div> Setiembre - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'
                    +'<div class="mes mes9 octubre"><div> Octubre - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'
                    +'<div class="mes mes10 noviembre"><div> Noviembre - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'
                    +'<div class="mes mes11 diciembre"><div> Diciembre - '+i+'</div><div style="display: flex;"> '+dias+' </div><div class="dia-mes"></div> </div>'

                    +"</div> </div>");

                $('.aa').append('<option value="'+i+'">'+i+'</option>');
                // $('.dia-mes').append('<div class="selection-item "> D</div>');
            
            }

            $('.selection-item').attr('style',"width: calc(14.2857% - 32px);height: 2.5rem;");
            $('.dia-mes').attr('style',"display: flex; flex-wrap: wrap;");

            $('.popup header').append('<select id="smes" class="select_mes" kk="'+anoi+'">'+
                                        '<option value="0">enero</option>'+
                                        '<option value="1">febrero</option>'+
                                        '<option value="2">marzo</option>'+
                                        '<option value="3">abril</option>'+
                                        '<option value="4">mayo</option>'+
                                        '<option value="5">junio</option>'+
                                        '<option value="6">julio</option>'+
                                        '<option value="7">agosto</option>'+
                                        '<option value="8">setiembre</option>'+
                                        '<option value="9">octubre</option>'+
                                        '<option value="10">noviembre</option>'+
                                        '<option value="11">diciembre</option>'+
                                        '</select>');

            $('.select_mes').on('change', function (e) {
                 var mes = this.value;
                 var ano = $(this).attr('kk');
                 $('.mes').hide();
                 $('.'+ano+' .mes'+mes).show();

                 $('.aa').attr('qq',mes);
            });

            $('.aa').on('change', function (e) {
                 var ano = this.value;
                 var mes = $(this).attr('qq');
                 $('.mes').hide();
                 $('.'+ano+' .mes'+mes).show();

                 $('.select_mes').attr('kk',ano);
            });

            $('.selection-item').each(function(i, obj) {
                var here = list[i];
                var shere = here.item.split("/");
                var i_dia = shere[0];
                var i_mes = shere[1];
                var i_ano = shere[2];

                var date = new Date();
                var ff = new Date(i_mes+' '+i_dia+', '+i_ano);
                var y = ff.getUTCDay();

                if(i_dia=='1'){
                  
                 
                 for (var j = 0 ; j <y; j++) {
                    $("."+i_ano+" .cuerpo .mes"+(i_mes-1)+" .dia-mes").append('<div  style=" '+
                        'width: calc(14.2857% - 32px);font-size: 16px;line-height: 50px;'+
                        'background-color: white;'+
                        'padding: 0px 16px;height: 2.5rem;"> </div>');
                 }
                }

                $(this).appendTo("."+i_ano+" .cuerpo .mes"+(i_mes-1)+" .dia-mes");

            });
                //mostrar el mes acual si no hay valor por defecto
                $('.mes').hide();
                //alert('.'+ac+' .mes'+mc);
                $('.'+ac+' .mes'+mc).show();
             

    		return def.then(function (delivery) {
                return delivery;
            });
    	},

    });

});