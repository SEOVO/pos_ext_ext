odoo.define('pos_ext_ext.chrome', function (require) {
    "use strict";

    var chrome = require('point_of_sale.chrome');
    var pos_delivery_widget = require('pos_ext_ext.widgets');

    chrome.Chrome.include({

        init: function () {
            this.widgets.push({
                'name':     'delivery_name',
                'widget':   pos_delivery_widget.DeliveryNameWidget,
                'replace':  '.placeholder-DeliveryNameWidget',
            });
            return this._super(arguments[0], {});
        },

    });

});