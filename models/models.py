# -*- coding: utf-8 -*-

from odoo import models, fields, api

class PosOrderjs(models.Model):
    _inherit = 'pos.order'
    delivery_date = fields.Date(string="Fecha de Envio")
    @api.model
    def _order_fields(self, ui_order):
        res = super()._order_fields(ui_order)
        fdate = ui_order.get('delivery_date', False)
        if fdate:
            fdate=str(fdate)
            res['delivery_date'] = fdate
        return res

